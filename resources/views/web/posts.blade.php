@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1> Lista de articulos </h1>

            @foreach($posts as $post)
                <div class="card mb-2">
                    <div class="card-header">
                        <h5 class="card-title">{{ $post->name }}</h5>
                    </div>
                    <div class="card-body">
                        @if($post->file)
                        <img src="{{ $post->file }}" alt="" class="img-fluid">
                        @endif
                        {{ $post->excerpt }}
                    <a href="{{ route('post', $post->slug) }}" class="float-right">Leer más.. </a>
                    </div>
                </div>
            @endforeach

            {{ $posts->render() }}
        </div>
    </div>
</div>


@endsection